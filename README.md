# get-runner

A wrapper around GET to simplify configuration and execution.

## Quickstart

**Warning**: This has been tested only against a [sandbox](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/) GCP project. If run against a shared project, there are currently no guarantees it won't step on someone elses toes.

1. Install this gem, or run it from a checkout of the repository
1. Ensure your developer license is saved in 1password, and tagged with `get_runner_license_key`
2. Create a configuration file in `~/.config/get-runner/config.toml`
   ```toml
   [global]
   cloud_provider="gcp"
   architecture = "1k"
   domain = "example-com" # This should be the name of the domain in Google Cloud DNS, not the DNS name. Fun!
   letsencrypt_email = "bar@example.com"

   [environment]
   project = "PROJECT_NAME"
   geo_deployment = "foo-geo"
   primary_zone="us-central1-a"
   secondary_zone="us-west1-a"
   machine_image="ubuntu-2204-lts"
   ``` 
   4. Run get-runner install --work-dir=PATH_TO_A_DIR_TO_STORE_CONFIG
   5. wait a bit

## Requirements
1. Container tool compatible with docker cli (tested using moby in rancher-desktop)
2. gcloud authenticated with project
3. If using DNS, it needs to be registered in the same project in Google Domains.

## Installation

1. Install the gem
2. Install the dependendices from .tool-versions with get-runner dependencies
3. Install a container tool (currently only tested against rancher-desktop) 
 
## Usage

1. Populate ~/.config/get-runner/config.toml
2. Install gcloud, and set it up to authenticate to your project (or perform the next steps via the webui)
3. Run the prepare script to create your service account
4. run get-runner install

### Configuration file

#### Environment
| Option | Type | Description |
| ------ | ---- | ----------- |
| package_download_url | String | URL to download a package from. |

### Secrets

This will create secrets in your personal 1 password vault if they don't exist. Some secrets you will need to create yourself.

| Tag | Field | Required | Description |
| --- | ---- | -------- | ----------- |
| get_runner_gitlab_com_pat | password | No | If you want to download packages from job artifacts via the `package_download_url`, this should be a PAT entry with at least `read_api` permissions. |
| get_runner_license_key | license key | Yes | The ultimate GitLab license to use for your instance. NOTE: get-runner assumes you are using a developer license, and connects to the developer license instance. |

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/get-runner. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/get-runner/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Get::Runner project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/get-runner/blob/main/CODE_OF_CONDUCT.md).
