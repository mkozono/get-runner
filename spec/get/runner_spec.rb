# frozen_string_literal: true

RSpec.describe Get::Runner do
  it "has a version number" do
    expect(Get::Runner::VERSION).not_to be nil
  end
end
