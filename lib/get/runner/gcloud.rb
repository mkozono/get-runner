# frozen_string_literal: true

require "thor"

module Get
  module Runner
    class Gcloud < Thor
      include Thor::Actions

      REQUIRED_ROLES = %w[
        compute.admin
        container.admin
        dns.admin
        iam.serviceAccountAdmin
        iam.serviceAccountUser
        storage.admin
        storage.objectAdmin
      ].freeze

      class_option :project, type: :string

      no_commands do
        def gcloud
          return "gcloud" if options[:project].nil?

          "gcloud --project #{options[:project]}"
        end
      end

      desc "service_account_email", "Fetch the service account email address"
      def service_account_email
        run(
          %(#{gcloud} iam service-accounts list --filter "name:get-runner" --format='table[no-heading](email)'),
          capture: true
        ).chomp
      end

      desc "add_roles", "Set the required roles"
      def add_roles
        command = %(#{gcloud} projects add-iam-policy-binding)
        REQUIRED_ROLES.each do |role|
          run(
            %(#{command} "#{options[:project]}" --member="serviceAccount:#{service_account_email}" --role="roles/#{role}")
          )
        end
      end

      desc "services", "Enable the required services"
      def services
        %w[cloudresourcemanager compute iam dns].each do |api|
          run("#{gcloud} services enable #{api}.googleapis.com")
        end
      end
    end
  end
end
