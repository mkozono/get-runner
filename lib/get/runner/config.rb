# frozen_string_literal: true

require "securerandom"

module Get
  module Runner
    class Config
      attr_reader :global, :environment, :architecture, :user_arch_file

      def initialize(config_file, user_arch = nil)
        config = TomlRB.load_file(config_file)

        @global = config["global"]
        @global["dns_name"] = dns_name
        @environment = config["environment"]
        # Generate a token for container registry replication
        @environment["crt"] = SecureRandom.base64(32)

        @user_arch_file = user_arch

        begin
          arch_name = global["architecture"]
          @architecture = architectures[arch_name][global["cloud_provider"]]
        rescue NoMethodError
          abort("#{arch_name} is not a valid architecture")
        end
      end

      def architectures
        architectures = TomlRB.load_file(File.join(File.dirname(__FILE__), "../../../conf/architectures.toml"))
        architectures.merge!(TomlRB.load_file(user_arch_file)) if !user_arch_file.nil? && File.exist?(user_arch_file)

        architectures
      end

      def to_h
        {
          global:,
          environment:,
          architecture:
        }
      end

      def to_json(obj = nil)
        to_h.to_json(obj)
      end

      def generate(work_dir, tool)
        mkdirs(work_dir, tool)
        jsonnet(work_dir, tool)
      end

      def project
        environment["project"]
      end

      private

      def mkdirs(work_dir, tool)
        # TODO: There is a slicker way to do this, since FileUtils.mkdir_p accepts an array of directories to create
        FileUtils.mkdir_p("#{work_dir}/#{tool}/primary")
        FileUtils.mkdir_p("#{work_dir}/#{tool}/secondary")
        FileUtils.mkdir_p("#{work_dir}/terraform/shared")
      end

      def jsonnet(work_dir, tool)
        %(
            jsonnet --ext-str configuration='#{to_json}'
                    -m #{work_dir}/#{tool}
                    -S jsonnet-configs/#{tool}.jsonnet
          ).gsub(/\s+/, " ")
      end

      def dns_name
        %x(gcloud dns managed-zones describe #{@global["domain"]} --format='table[no-heading](dnsName)').chomp(".\n")
      end
    end
  end
end
