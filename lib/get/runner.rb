# frozen_string_literal: true

# require_relative "runner/ansible"
require_relative "runner/config"
require_relative "runner/environment"
require_relative "runner/gcloud"
require_relative "runner/prechecks"
require_relative "runner/version"

require "fileutils"
require "json"
require "thor"
require "toml-rb"

# TODO: We don't need this for production
require "pry"

module Get
  module Runner
    class Error < StandardError; end

    class App < Thor
      include Thor::Actions
      def self.exit_on_failure?
        true
      end

      class_option :config_dir, default: "#{Dir.home}/.config/get-runner"
      class_option :config_file, default: "config.toml"
      class_option :arch_file, default: "architectures.toml"
      class_option :work_dir, desc: "Set this if you want your configuration to persist between runs"
      class_option :get_version, desc: "The version of GET to run", default: "3.2.2"
      class_option :vault, desc: "1password vault", default: "Personal"
      class_option :debug, desc: "Debug get-runner", type: :boolean, default: false

      register(
        Get::Runner::Prechecks, "prechecks", "prechecks", "Check that the system has the necessary components installed"
      )

      no_commands do
        def configuration
          return @configuration if defined?(@configuration)

          config_dir = options[:config_dir]

          @configuration = Get::Runner::Config.new(
            "#{config_dir}/#{options[:config_file]}", "#{config_dir}/#{options[:arch_file]}"
          )
        end

        def config_environment
          configuration.environment
        end

        def gcloud_cmd
          %(gcloud --project "#{configuration.project}")
        end

        def sa_email
          # Invoke does not rerun a command that ran before. And memoizing the
          # result in an instance variable did not work.
          # invoke "get:runner:gcloud:service_account_email", [], project: configuration.project
          # TODO: Pass in configuration.project
          Get::Runner::Gcloud.new.service_account_email
        end

        def op
          %(op --vault "#{options[:vault]}")
        end

        def instances
          return @instances if defined?(@instances)

          filter = "name:#{config_environment["geo_deployment"]}*"
          @instances = JSON.parse(
            run(
            %(#{gcloud_cmd} compute instances list --filter='#{filter}' --format=json),
              capture: true, verbose: false
            )
          )
        end

        def service_key_file_id
          return @service_key_file_id if defined?(@service_key_file_id)

          items = JSON.parse(run(
                               %(#{op} item list --tags get_runner_service_key --format json),
                               capture: true,
                               verbose: false
                             ))

          if items.length > 1
            abort("Multiple items found in 1password with tag get_runner_service_key. Not sure what to do")
          end

          @service_key_file_id = items.empty? ? nil : items[0]["id"]
        end

        def docker_command(cmd, env_file = nil)
          <<~EO_COMMAND
            docker run -it --rm \
            -v #{File.realdirpath(options[:work_dir])}:/srv/inventories \
            -v #{File.join(File.dirname(__FILE__), "../../", "get")}:/srv/scripts \
            -e PREFIX="#{config_environment["geo_deployment"]}" \
            -e LOCATION="#{config_environment["primary_zone"].slice(0..-3)}" \
            -e PROJECT="#{configuration.project}" \
            #{"--env-file #{env_file}" unless env_file.nil?} \
            registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:#{options[:get_version]} #{cmd}
          EO_COMMAND
        end

        def run_docker(command)
          Dir.mktmpdir do |tmpdir|
            invoke "get:runner:environment:create", [], env_file: "#{tmpdir}/env", vault: options[:vault]
            run docker_command(command, "#{tmpdir}/env")
          end
        end
      end

      desc "test", "test"
      def test
        puts "ONE: #{sa_email}"
        puts "TWO: #{sa_email}"
      end

      desc "config", "Generate the configuration"
      def config
        %w[terraform ansible].each do |tool|
          run configuration.generate(options[:work_dir], tool), verbose: false
        end
      end

      desc "status", "Show the status of the instances"
      def status
        puts(instances.collect { |instance| "#{instance["name"]} - #{instance["status"]}" })
      end

      desc "start", "Ensure all instances are started"
      def start
        instances.each do |instance|
          run(%(#{gcloud_cmd} compute instances start "#{instance["name"]}" --zone "#{instance["zone"]}"))
        end
      end

      desc "stop", "Ensure all instances are stopped"
      def stop
        instances.each do |instance|
          run(%(#{gcloud_cmd} compute instances stop "#{instance["name"]}" --zone "#{instance["zone"]}"))
        end
      end

      desc "check", "show me the config"
      def check
        puts configuration.to_json
      end

      desc "service_account_key", "Create the service account key"
      def service_account_key
        title = "get-runner service key file"
        Dir.mktmpdir do |dir|
          run(%(#{gcloud_cmd} iam service-accounts keys create "#{dir}/key.json" --iam-account="#{sa_email}"))
          run(
            %(#{op} document create --tags get_runner,get_runner_service_key "#{dir}/key.json" --title "#{title}")
          )
        end
      end

      desc "service_account", "Creates the service account and stores the key in 1password"
      def service_account
        if service_key_file_id.nil?
          puts "Service account key not found. Will create"
          run(%(#{gcloud_cmd} iam service-accounts create "get-runner" --display-name="get-runner")) if sa_email.empty?
          # Add the roles to the account. This does not error if it already has the role
          invoke "get:runner:gcloud:add_roles", [], project: configuration.project
          invoke :service_account_key
          invoke "get:runner:gcloud:services", [], project: configuration.project
        else
          puts "Service key found in 1password, assuming this has been setup"
        end
      end

      desc "ssh_key", "generate the ssh key"
      def ssh_key
        items = JSON.parse(`#{op} item list --tags get_runner_ssh_key --format json`)
        case items.length
        when 0
          puts "Creating SSH key"
          run("#{op} item create --category ssh --title 'get-runner ssh-key' --tags get_runner,get_runner_ssh_key")
        when 1
          puts "SSH key already exists, assuming nothing to do"
        else
          abort("Multiple items found in 1password with tag get_runner_ssh_key. Not sure what to do")
        end
      end

      desc "prepare", "Run steps to prepare the environment"
      def prepare
        invoke :service_account
        invoke :ssh_key
      end

      desc "install", "Install the configuration"
      method_option :skip_config, type: :boolean, default: false
      method_option :skip_prepare, type: :boolean, default: false
      def install
        invoke :config unless options[:skip_config]
        invoke :prechecks
        invoke :prepare unless options[:skip_prepare]

        command = options[:debug] ? "bash" : "/srv/scripts/install"
        run_docker(command)
      end

      desc "uninstall", "Uninstall the configuration"
      def uninstall
        command = "/srv/scripts/uninstall"
        run_docker(command)
      end

      desc "dependencies", "Install the required dependencies"
      method_option :tool, default: "mise"
      def dependencies
        inside File.dirname(__FILE__) do
          run("#{options[:tool]} install")
        end
      end

      desc "architectures", "List the known architectures"
      def architectures
        puts "Name | Vendor | Description"
        configuration.architectures.each_pair do |arch, config|
          config.each_key do |vendor|
            message = config[vendor].key?("description") ? config[vendor]["description"] : nil
            puts "#{arch} | #{vendor} | #{message}"
          end
        end
      end

      desc "gcloud", "Commands to interact with Google Cloud"
      subcommand "gcloud", Get::Runner::Gcloud

      desc "environment", "Commands to setup the container environment"
      subcommand "environment", Get::Runner::Environment

      # desc "ansible", "Commands to interact with Ansible"
      # subcommand "ansible", Get::Runner::Ansible
    end
  end
end

Get::Runner::App.start
