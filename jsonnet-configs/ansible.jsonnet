local configuration = std.parseJson(std.extVar('configuration'));
local plugin = 'gcp_compute';
local node_filter(site) = [
    "labels.gitlab_node_prefix = " + configuration.environment.geo_deployment + "-" + site,
];
local os_version = "ubuntu-jammy";
local download_url(project, jobid) =
  if project != "" then
    "https://gitlab.com/api/v4/projects/" + project + "/jobs/" + jobid + "/artifacts/pkg/" + os_version + "/gitlab.deb"
  else
    ""
  ;

local package_repo = std.get(configuration.environment, 'repo', default=null);

local external_url(site) =
    if std.objectHas(configuration.global, "dns_name") then
        "https://" + site + "." + configuration.global.dns_name
    else
        ""
;

local geo_primary_registry_url = "https://registry." + configuration.global.dns_name;

local main_vars = {
    all: {
        vars: {
            cloud_provider: configuration.global.cloud_provider,
            container_registry_token: configuration.environment.crt,
            gcp_project: configuration.environment.project,
            external_ssl_source: 'letsencrypt',
            external_ssl_letsencrypt_issuer_email: configuration.global.letsencrypt_email,
            system_packages_auto_security_upgrade: false,
            service_account_file: "/tmp/creds.json",
            gitlab_root_password: "{{ lookup('env', 'GITLAB_ROOT_PASSWORD') }}",
            pgbouncer_password: "{{ lookup('env', 'PGBOUNCER_PASSWORD_ENC') | b64decode }}",
            consul_password: "{{ lookup('env', 'CONSUL_PASSWORD_ENC') | b64decode }}",
            praefect_internal_token: "{{ lookup('env', 'PRAEFECT_INTERNAL_TOKEN_ENC') | b64decode }}",
            redis_password: "{{ lookup('env', 'REDIS_PASSWORD_ENC') | b64decode }}",
            postgres_password: "{{ lookup('env', 'POSTGRES_PASSWORD_ENC') | b64decode }}",
            gitlab_license_text: "{{ lookup('ansible.builtin.env', 'GITLAB_LICENSE_TEXT') }}",
            gitlab_rails_custom_config_file: "/srv/scripts/ansible/custom/gitlab_rails.rb.j2",
            gitlab_shell_ssh_daemon: "gitlab-sshd"
        } + (
            if std.objectHas(configuration.environment, 'gitlab_version') then
                { gitlab_version: configuration.environment.gitlab_version }
            else if std.objectHas(configuration.environment, 'package_job_id') then
                { gitlab_deb_download_url: download_url(std.get(configuration.environment, "package_project", ""), std.get(configuration.environment, "package_job_id", "")) }
            else if package_repo == "nightly" then
                { gitlab_repo_script_url: "https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh"}
            else if std.objectHas(configuration.environment, 'package_download_url') then
                { 
                    gitlab_deb_download_url: configuration.environment.package_download_url,
                    gitlab_deb_force_install: true 
                }
            else
                {  }
            )
          } };

local primary_vars = main_vars + {
    all+: {
        vars+: {
            prefix: configuration.environment.geo_deployment + "-primary",
            external_url: external_url("primary"),
            geo_primary_external_url: external_url("primary"),
            geo_primary_site_group_name: "geo_primary_site",
            geo_primary_site_name: "Primary Site",
            container_registry_external_url: geo_primary_registry_url,
        } + (
            if std.objectHas(configuration.environment, 'geo_primary_internal_url') then
                {  geo_primary_internal_url: configuration.environment.geo_primary_internal_url }
            else
                {  }
            )
    }
};

local secondary_vars = main_vars + {
    all+: {
        vars+: {
            prefix: configuration.environment.geo_deployment + "-secondary",
            external_url: external_url("secondary"),
            geo_secondary_external_url: external_url("secondary"),
            geo_secondary_site_group_name: "geo_secondary_site",
            geo_secondary_site_name: "Secondary Site",
            container_registry_external_url: "https://registry.secondary." + configuration.global.dns_name,
            geo_primary_registry_url: geo_primary_registry_url,
        } + (
            if std.objectHas(configuration.environment, 'geo_secondary_internal_url') then
                { geo_secondary_internal_url: configuration.environment.geo_secondary_internal_url }
            else
                {}
            )
    }
};

local keyed_groups = [
    { key: 'labels.gitlab_node_type', separator: '' },
    { key: 'labels.gitlab_node_level', separator: '' },
    { key: 'labels.gitlab_geo_site', separator: '' },
    { key: 'labels.gitlab_geo_full_role', separator: '' },
];
local scopes = [ 'https://www.googleapis.com/auth/compute' ];
local hostnames = [ 'name' ];
local compose = { ansible_host: 'networkInterfaces[0].accessConfigs[0].natIP' };

local main_gcp = {
  auth_kind: "serviceaccount",
  plugin: plugin,
  projects: [ configuration.environment.project ],
  keyed_groups: keyed_groups,
  scopes: scopes,
  hostnames: hostnames,
  compose: compose
};

{
    'primary/geo.gcp.yml': std.manifestYamlDoc(
            main_gcp + { filters: node_filter('primary') },
            indent_array_in_object=true, quote_keys=false),
    'secondary/geo.gcp.yml': std.manifestYamlDoc(
            main_gcp + { filters: node_filter('secondary') },
            indent_array_in_object=true, quote_keys=false),
    'primary/vars.yml': std.manifestYamlDoc(
            primary_vars,
            indent_array_in_object=true, quote_keys=false),
    'secondary/vars.yml': std.manifestYamlDoc(
            secondary_vars,
            indent_array_in_object=true, quote_keys=false),

}
