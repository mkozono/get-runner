local configuration = std.parseJson(std.extVar('configuration'));
local region_from_zone(zone) =
    local components = std.split(zone, '-');
    std.join('-', [components[0], components[1]]);

local terraform_env = {
    geo_deployment: configuration.environment.geo_deployment,
    object_storage_buckets_force_destroy: true,
    project: configuration.environment.project,
};

local sa_prefix(prefix, site) =
    local words = [std.substr(i, 0, 1) for i in std.split(prefix, '-')];
    std.join("", words) + "-" + site;

local terraform_environment = {
    module: {
        gitlab_ref_arch_gcp: {
                // terraform treats relative paths differently than absolute paths, if we go with absolute we'll get the error
                // The given source directory for module.gitlab_ref_arch_gcp.module.redis_cache would be outside of its containing package....
                // Probably fixable, but not today
                source: "../../../../gitlab-environment-toolkit/terraform/modules/gitlab_ref_arch_gcp",
                prefix: "${var.prefix}",
                project: "${var.project}",
                service_account_prefix: "${var.sa_prefix}",
                machine_image: "${var.machine_image}",
                geo_site: "${var.geo_site}",
                haproxy_external_external_ips: ["${google_compute_address.external_ip.address}"],
        } + configuration.architecture.machines
    },
    variable: {
        geo_deployment: {},
        geo_site: {},
        machine_image: {},
        object_storage_buckets_force_destroy: {},
        prefix: {},
        project: {},
        region: {},
        //ssh_public_key_file: {},
        sa_prefix: {},
        zone: {}
    },
    output: {
     gitlab_ref_arch_gcp: {
         value: "${module.gitlab_ref_arch_gcp}"
     }
    }
};

local backend_config(state_prefix) =  {
    terraform: {
        backend: {
            gcs: {
                bucket: configuration.environment.geo_deployment + "-get-terraform-state",
                prefix: state_prefix

            }
        }
    },
};

local provider_config = {
    provider: {
        google: {
            project: "${var.project}",
            region: "${var.region}",
            zone: "${var.zone}"
        }
    },
};

local site_main(state_prefix) = {
        resource: {
        google_compute_address: {
            external_ip: {
                name: "${var.prefix}-external",
                region: "${var.region}"
            }
        }
    },
    output: {
        external_ip_addr: {
            value: "${google_compute_address.external_ip.address}"
        }
    }
} + backend_config(state_prefix) + provider_config;

local shared_main = backend_config("shared") + provider_config;

local primary_terraform = {
    region: region_from_zone(configuration.environment.primary_zone),
    zone: configuration.environment.primary_zone,
    geo_site: 'geo-primary-site',
    machine_image: configuration.environment.machine_image,
    prefix: configuration.environment.geo_deployment + '-primary',
    sa_prefix: sa_prefix(configuration.environment.geo_deployment, "pri"),
} + terraform_env;

local primary_main = site_main(primary_terraform.prefix);

local secondary_terraform = {
    region: region_from_zone(configuration.environment.secondary_zone),
    zone: configuration.environment.secondary_zone,
    geo_site: 'geo-secondary-site',
    machine_image: configuration.environment.machine_image,
    prefix: configuration.environment.geo_deployment + '-secondary',
    sa_prefix: sa_prefix(configuration.environment.geo_deployment, "sec"),
} + terraform_env;

local secondary_main = site_main(secondary_terraform.prefix);

local shared_environment = [
    {
        data: {
            google_dns_managed_zone: {
                domainname: {
                    name: "${var.domain}",
                }
            }
        }
    },
    {
        resource: {
            google_dns_record_set: {
                primary: {
                    name: "primary.${data.google_dns_managed_zone.domainname.dns_name}",
                    managed_zone: "${data.google_dns_managed_zone.domainname.name}",
                    type: "A",
                    rrdatas: ["${var.primary_ip}"]
                }
            }
        }
    },
    {
        resource: {
            google_dns_record_set: {
                registry: {
                    name: "registry.${data.google_dns_managed_zone.domainname.dns_name}",
                    managed_zone: "${data.google_dns_managed_zone.domainname.name}",
                    type: "A",
                    rrdatas: ["${var.primary_ip}"]
                }
            }
        }
    },

    {
        resource: {
            google_dns_record_set: {
                secondary: {
                    name: "secondary.${data.google_dns_managed_zone.domainname.dns_name}",
                    managed_zone: "${data.google_dns_managed_zone.domainname.name}",
                    type: "A",
                    rrdatas: ["${var.secondary_ip}"]
                }
            }
        }
    },
    {
        resource: {
            google_dns_record_set: {
                secondary_registry: {
                    name: "registry.secondary.${data.google_dns_managed_zone.domainname.dns_name}",
                    managed_zone: "${data.google_dns_managed_zone.domainname.name}",
                    type: "A",
                    rrdatas: ["${var.secondary_ip}"]
                }
            }
        }
    },
];

local shared_variables_tf = {
    variable: {
        domain: {},
        primary_region: {},
        primary_ip: {},
        secondary_region: {},
        secondary_ip: {},
        project: {},
        region: {},
        zone: {},
    },
};

local shared_variables = {
  domain: configuration.global.domain,
  primary_region: region_from_zone(configuration.environment.primary_zone),
  secondary_region: region_from_zone(configuration.environment.secondary_zone),
  project: configuration.environment.project,
  region: region_from_zone(configuration.environment.primary_zone),
  zone: configuration.environment.primary_zone,
};

{
    'primary/environment.tf.json': std.manifestJsonEx(terraform_environment, ' '),
    'primary/main.tf.json': std.manifestJsonEx(primary_main, ' '),
    'primary/terraform.tfvars.json': std.manifestJsonEx(primary_terraform, '  '),
    'secondary/terraform.tfvars.json': std.manifestJsonEx(secondary_terraform, '  '),
    'secondary/environment.tf.json': std.manifestJsonEx(terraform_environment, ' '),
    'secondary/main.tf.json': std.manifestJsonEx(secondary_main, ' '),
    'shared/main.tf.json': std.manifestJsonEx(shared_main, ' '),
    'shared/environment.tf.json': std.manifestJsonEx(shared_environment, ' '),
    'shared/terraform.tfvars.json': std.manifestJsonEx(shared_variables, ' '),
    'shared/variables.tf.json': std.manifestJsonEx(shared_variables_tf, ' '),
}
